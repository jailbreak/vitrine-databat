import { ArrayModel, ObjectModel } from "objectmodel"

import { fetchPageData, loadFetcher } from "../fetchers"
import { justOneIsDefined } from "../utils"
import { SourceDiscourse, SourceGitlab, SourceHtml } from "./source"
import Zone from "./zone"

const Banner = new ObjectModel({
	classes: [String],
	url: [String],
}).as("Banner")

const CallToActionButton = new ObjectModel({
	title: String,
	url: String,
}).as("CallToActionButton")

const PageModel = new ObjectModel({
	appBarTitle: [String],
	callToActionButton: [CallToActionButton],
	banner: [Banner],
	slug: String,
	source: [SourceDiscourse, SourceGitlab, SourceHtml, undefined],
	subTitle: [String],
	title: [String],
	zones: [ArrayModel(Zone)],
})
	.assert((m) => justOneIsDefined([m.zones, m.source]), "zones or source should be defined")
	.as("Page")

export default class Page extends PageModel {
	async fetchData(config, context) {
		async function getSourceData(source) {
			const fetcher = loadFetcher(source.type)
			const sourceRenderType = fetcher.getSourceRenderType(source)
			const data = await fetchPageData(sourceRenderType, fetcher, source, config, context)
			return { sourceRenderType, data }
		}
		if (this.source) {
			return await getSourceData(this.source)
		}
		if (this.zones) {
			return await Promise.all(this.zones.map((zone) => getSourceData(zone.source)))
		}
		throw new Error("zones or source should be defined, this line should never be reached")
	}
}
