import { ArrayModel, ObjectModel } from "objectmodel"

import AppBar from "./appBar"
import Page from "./page"

const NavbarButtons = new ObjectModel({
	title: String,
	url: String,
})

const ConfigModel = new ObjectModel({
	version: 2,
	appBar: AppBar,
	pages: ArrayModel(Page),
	filterTag: [String],
	navbarButtons: [ArrayModel(NavbarButtons)],
}).as("Config")

export default class Config extends ConfigModel {
	findPageBySlug(slug) {
		return this.pages.find((page) => page.slug === slug)
	}
}
