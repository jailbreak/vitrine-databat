import { ObjectModel } from "objectmodel"

import { SourceDiscourse, SourceGitlab, SourceHtml } from "./source"
import { ButtonWithPageSlug, ButtonWithUrl } from "./button"
// import ButtonWithPageSlug from "./buttonWithPageSlug"

export default new ObjectModel({
	title: [String],
	source: [SourceDiscourse, SourceGitlab, SourceHtml],
	button: [ButtonWithPageSlug, ButtonWithUrl, undefined], // https://objectmodel.js.org/#doc-multiple-types
}).as("Zone")
