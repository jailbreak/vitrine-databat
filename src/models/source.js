import { ObjectModel } from "objectmodel"

import { justOneIsDefined } from "../utils"
import { PositiveInteger } from "./common"

const Source = new ObjectModel({
	type: String,
})

export const SourceDiscourse = Source.extend({
	type: "discourse",
	url: String,
	category: [PositiveInteger],
	topic: [PositiveInteger],
	limit: [PositiveInteger],
})
	.assert((s) => justOneIsDefined([s.category, s.topic]), "only category or topic should be defined")
	.as("SourceDicourse")

export const SourceGitlab = Source.extend({
	type: "gitlab",
	url: String,
	group: [String],
	project: [String],
})
	.assert((s) => justOneIsDefined([s.group, s.project]), "only group or project should be defined")
	.as("SourceGitlab")

export const SourceHtml = Source.extend({
	type: "html",
	content: String,
	raw: [Boolean],
	title: [String],
})
	.defaultTo({ raw: false })
	.as("SourceHtml")
