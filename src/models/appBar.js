import { ObjectModel } from "objectmodel"

export default new ObjectModel({
	title: new ObjectModel({
		text: String,
		classes: [String],
	}),
	hamburgerMenu: new ObjectModel({
		color: [String],
	}),
	tabs: new ObjectModel({
		classes: [String],
		color: [String],
	}),
}).as("AppBar")
