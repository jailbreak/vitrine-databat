import { FetchError } from "./errors.js"
import fetchRetry from "fetch-retry"

export async function fetchUrl(url, context) {
	const fetchWithRetry = fetchRetry(context.fetch, {
		retryOn: (attempt, error, response) => error !== null || !response.ok,
	})
	const info = { url }
	let response
	try {
		response = await fetchWithRetry(url)
		if (!response.ok) {
			throw response
		}
	} catch (error) {
		throw new FetchError({
			message: `Error while fetching URL`,
			info,
		}).withCause(error)
	}
	try {
		return await response.json()
	} catch (error) {
		throw new FetchError({
			message: "Error while parsing JSON",
			info,
		}).withCause(error)
	}
}
