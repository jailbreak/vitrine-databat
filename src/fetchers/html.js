export function getSourceRenderType(source) {
	return "content"
}

export async function fetchCardsPageData(source, config, context) {
	throw new Error("Not implemented: html fetcher only returns content, not cards")
}

export async function fetchContentPageData(source, config, context) {
	return {
		title: source.title,
		html: source.content,
		raw: source.raw,
	}
}
