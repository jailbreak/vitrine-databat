// tailwind.config.js
module.exports = {
	future: {
		defaultLineHeights: true,
		purgeLayersByDefault: true,
		removeDeprecatedGapUtilities: true,
		standardFontWeights: true,
	},
	theme: {
		extend: {
			colors: {
				primary: "#7BA5D0",
				secondary: "#E10D1A",
			},
		},
	},
	variants: {},
	plugins: [],
}
