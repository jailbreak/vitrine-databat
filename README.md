# Vitrine

Vitrine (French for "showcase") is a free and open source tool to easily generate a website using content retrieved from third-party pages.
Originally, its main use case was for a hackathon landing page, to showcase its projects and participants, datasets, tools, etc.
Vitrine can also be used for any other purpose.

## Current instances

- 2018 & 2020 [DataFin Hackathon](https://datafin.fr/) ([repo](https://git.en-root.org/eraviart/vitrine/))
- 2019 [AGDIC Hackathon](https://agdic-hackathon.frama.io/) ([repo](https://framagit.org/agdic-hackathon/agdic-hackathon.frama.io))
- 2020 [DataBât Hackathon](https://databat.ademe.fr/) ([repo](https://gitlab.com/jailbreak/vitrine-databat/))

## Features

### Discourse and Gitlab as a CMS

Vitrine was inspired by other **content managing systems (CMS)** such as [Hackdash](https://www.hackdash.org/) or [Wordpress](https://www.wordpress.org) but instead of having its own back office / admin panel, it's based on [Discourse](https://www.discourse.org/) and [GitLab](https://about.gitlab.com/). This allows you to take advantage of the rich features offered by these tools, especially for community management, while at the same time using them as a CMS to edit collaboratively and in near-real time the content displayed in Vitrine.

### Static / Dynamic Hybrid

Vitrine can be thought as an hybrid between a **static site generators (SSG)** — such as [Jekyll](https://www.jekyllrb.com/), [Gatsby](https://www.gatsbyjs.com/) or [Hugo](https://www.gohugo.io/), which enables you to export and deploy a Vitrine website to [Github](https://pages.github.com/)/[Gitlab Pages](https://docs.gitlab.com/ee/user/project/pages/) or [Netlify](https://www.netlify.com/) — and a classic website which **dynamically pulls data in real time from APIs** (currently Discourse and Gitlab are supported but others can be considered).

### Easy to configure, free to deploy

Many elements of a Vitrine website can be customized by editing **a single YAML config file**, which is much easier than modifying the code itself. Styling elements are also managed with [Tailwind](https://www.tailwindcss.com/) instead of complex CSS.

The combination of all these features means that a person without software development skills is able to:

1. Fork,
2. Customize,
3. Deploy,
4. Manage the content from Discourse and/or Gitlab,

in a few clicks, without spending one cent.

## Requirements

To deploy your Vitrine website, you first need a working Discourse instance and access to its server to [configure it](#discourse-configuration).

Alternatively, or additionally, you can use repositories on a Gitlab instance, such as [Gitlab.com](https://www.gitlab.com), to manage the content of your Vitrine website.

## Customization

The easiest way to customize Vitrine is to fork this project and edit [`content/config.yml`](content/config.yml) directly from Gitlab.

## Deployment

Go to your project's **CI/CD** > **Pipelines** and click **Run pipeline**. The site can take a few minutes to build. When the pipeline is finished, go to **Settings** > **Pages** to make sure [GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/) is enabled on your project.

By default, your fork will be deployed on `https://<username>.gitlab.io/vitrine/`

## Installation

For more advanced uses, you should install and run Vitrine on your system before deploying your customizations. First, fork this repo, clone it on your system, then install Vitrine:

```bash
npm install
```

Finally, run Vitrine:

```bash
npm run dev
```

The app will be accessible in your browser at http://localhost:3000/. The application will reload every time `content/config.yml` or any file in `src/` are changed.

### Production

To build the application for a production use:

```bash
npm run export
```

This will generate a `__sapper__/export/` directory containing the static website.

See also [`.gitlab-ci.yml`](.gitlab-ci.yml) for an example.

## Discourse Configuration

### Enable CORS

Discourse must be configured to accept cross-origin resource sharing (CORS) requests coming from Vitrine.

In file `/var/discourse/containers/app.yml`, add line:

```yaml
  env:
    [...]
    DISCOURSE_ENABLE_CORS: true
```

Then in the Discourse admin web page `/admin/site_settings/category/security` fill field **cors origin** with the URL of your Vitrine website.

### Enable excerpts

By default Discourse does not return an `excerpt` key in the topics of the category endpoint. Vitrine needs them do display the content of those topics as cards.

In file `/var/discourse/containers/app.yml`, add line:

```yaml
run:
  - exec: echo "Beginning of custom commands"
  [...]
  - exec: rails r "SiteSetting.always_include_topic_excerpts=true"
  [...]
  - exec: echo "End of custom commands"
```

Alternatively, you can install [Topic List Previews plugin](https://meta.discourse.org/t/101646) to enable excerpts as well as other useful Discourse features.

### Rebuild app

Every time the `app.yml` is changed, you should rebuild the app, from the `/var/discourse/` directory:

```bash
./launcher rebuild app
```
